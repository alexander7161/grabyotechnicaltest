import PokerHand, { Result } from "./PokerHand.js";

describe("PokerHand", () => {
	describe("Invalid Hands", () => {
		it(`Missing Card`, () => {
			expect(() => {
				new PokerHand("AC 4S 5S 8C");
			}).toThrow();
		});
		it(`Missing space`, () => {
			expect(() => {
				new PokerHand("AC 4S 5S 8C3H");
			}).toThrow();
		});
		it(`Invalid card`, () => {
			expect(() => {
				new PokerHand("AC 4S 5S 8C 3HG");
			}).toThrow();
		});
	});
	describe("isStraight", () => {
		it(`JC TH 9C 8H 7D`, () => {
			const hand1 = new PokerHand("JC TH 9C 8H 7D");
			expect(hand1.isStraight()).toBeTruthy();
		});
		it(`AH 2D 3C 4C 5D`, () => {
			const hand1 = new PokerHand("AH 2D 3C 4C 5D");
			expect(hand1.isStraight()).toBeTruthy();
		});
		it(`TH JH QH KH AH`, () => {
			const hand1 = new PokerHand("TH JH QH KH AH");
			expect(hand1.isStraight()).toBeTruthy();
		});
		it(`TH JH QH KH AH`, () => {
			const hand1 = new PokerHand("TH JH 3H KH AH");
			expect(hand1.isStraight()).toBeFalsy();
		});
		it(`AH 2D 3C 7C 5D`, () => {
			const hand1 = new PokerHand("AH 2D 3C 7C 5D");
			expect(hand1.isStraight()).toBeFalsy();
		});
	});
	describe("isFlush", () => {
		it(`KD QD 9D 5D 3D`, () => {
			const hand1 = new PokerHand("KD QD 9D 5D 3D");
			expect(hand1.isFlush()).toBeTruthy();
		});
		it(`KH QH AH 5H 3H`, () => {
			const hand1 = new PokerHand("KH QH AH 5H 3H");
			expect(hand1.isFlush()).toBeTruthy();
		});
		it(`JC TH 9C 8H 7D`, () => {
			const hand1 = new PokerHand("JC TH 9C 8H 7D");
			expect(hand1.isFlush()).toBeFalsy();
		});
	});
	describe("getHandValue()", () => {
		it(`High Card`, () => {
			expect(new PokerHand("AC 4S 5S 8C 3H").getHandValue()).toBe("highCard");
		});
		it(`Pair`, () => {
			expect(new PokerHand("AC 4S 5S 8C AH").getHandValue()).toBe("pair");
		});
		it(`Two Pair`, () => {
			expect(new PokerHand("AC AS 5D 5C 4H").getHandValue()).toBe("2pairs");
		});
		it(`Three of a kind`, () => {
			expect(new PokerHand("AC AH AS 5C 4H").getHandValue()).toBe(
				"threeOfAKind"
			);
		});

		it(`Straight`, () => {
			expect(new PokerHand("JC TH 9C 8H 7D").getHandValue()).toBe("straight");
		});
		it(`Flush`, () => {
			expect(new PokerHand("KD QD 9D 5D 3D").getHandValue()).toBe("flush");
		});
		it(`Full House`, () => {
			expect(new PokerHand("KC KH KD 7C 7S").getHandValue()).toBe("fullHouse");
		});
		it(`Four of a kind`, () => {
			expect(new PokerHand("QC QH QD QS 5S").getHandValue()).toBe(
				"fourOfAKind"
			);
		});
		it(`Straight flush`, () => {
			expect(new PokerHand("3C 4C 5C 6C 7C").getHandValue()).toBe(
				"straightFlush"
			);
		});
		it(`Royal Flush`, () => {
			expect(new PokerHand("TC JC QC KC AC").getHandValue()).toBe(
				"straightFlush"
			);
			expect(new PokerHand("TC JC QC KC AC").score).toBe(12);
		});
	});
	describe("compareWith()", () => {
		describe("High Card", () => {
			it(`ties`, () => {
				const hand1 = new PokerHand("AC 4S 5S 8C 3H");
				const hand2 = new PokerHand("4S 5S 8C 3S AD");
				expect(hand1.compareWith(hand2)).toBe(Result.TIE);
			});
			it(`wins`, () => {
				const hand1 = new PokerHand("AC 4S 5S 8C KH");
				const hand2 = new PokerHand("4S 5S 8C 2S 3D");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 4S 5S 8C 6H");
				const hand2 = new PokerHand("4S 5S 8C AS AD");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("pair", () => {
			it(`ties`, () => {
				const hand1 = new PokerHand("AC 4S 5S 8C AH");
				const hand2 = new PokerHand("4S 5S 8C AS AD");
				expect(hand1.compareWith(hand2)).toBe(Result.TIE);
			});
			it(`wins`, () => {
				const hand1 = new PokerHand("AC 4S 5S 8C AH");
				const hand2 = new PokerHand("4S 5S 8C 2S AD");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 4S 5S 8C AH");
				const hand2 = new PokerHand("4S 5S 8C AS AD");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("twopair", () => {
			it(`ties`, () => {
				const hand1 = new PokerHand("AC AS 5D 5C 4H");
				const hand2 = new PokerHand("AH AD 5H 5S 4D");
				expect(hand1.compareWith(hand2)).toBe(Result.TIE);
			});
			it(`wins`, () => {
				const hand1 = new PokerHand("AC AH 5S 5C 4H");
				const hand2 = new PokerHand("AS AD 8C 2S 3D");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 4H 5S 8C AH");
				const hand2 = new PokerHand("4S 4S 8C AS AD");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("ThreeOfAKind", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("AC AH AS 5C 4H");
				const hand2 = new PokerHand("4S AD 8C 2S 3D");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 4H 5S 8C AH");
				const hand2 = new PokerHand("4S 4S 4C 3S AD");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Straight", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("JC TH 9C 8H 7D");
				const hand2 = new PokerHand("AC AH AS 5C 4H");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 4H 5S 8C AH");
				const hand2 = new PokerHand("2S 3S 4C 5S 6D");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Flush", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("KD QD 9D 5D 3D");
				const hand2 = new PokerHand("JC TH 9C 8H 7D");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2S 3S 4C 5S 6D");
				const hand2 = new PokerHand("KD QD 9D 5D 3D");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Full House", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("KC KH KD 7C 7S");
				const hand2 = new PokerHand("KS QD 9D 5D 3D");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("QC QH QD 7C 7S");
				const hand2 = new PokerHand("KC KH KD 7C 7S");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Four of a kind", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("QC QH QD QS 5S");
				const hand2 = new PokerHand("KC KH KD 7C 6S");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 2H 2D 2S 7S");
				const hand2 = new PokerHand("QC QH QD QS 5S");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Straight flush", () => {
			it(`wins`, () => {
				const hand1 = new PokerHand("3C 4C 5C 6C 7C");
				const hand2 = new PokerHand("KC KH KD 7C 6S");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it(`loses`, () => {
				const hand1 = new PokerHand("2C 2H 2D 2S 7S");
				const hand2 = new PokerHand("5C 6C 7C 8C 9C");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
		describe("Royal Flush", () => {
			it("win", () => {
				const hand1 = new PokerHand("TC JC QC KC AC");
				const hand2 = new PokerHand("5C 6C 7C 8C 9C");
				expect(hand1.compareWith(hand2)).toBe(Result.WIN);
			});
			it("loss", () => {
				const hand1 = new PokerHand("AC AS 5D 5C 4H");
				const hand2 = new PokerHand("TC JC QC KC AC");
				expect(hand1.compareWith(hand2)).toBe(Result.LOSS);
			});
		});
	});
});
