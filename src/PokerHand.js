const VALUES = [
	"2",
	"3",
	"4",
	"5",
	"6",
	"7",
	"8",
	"9",
	"T",
	"J",
	"Q",
	"K",
	"A"
];
const HAND_VALUES = [
	"highCard",
	"pair",
	"2pairs",
	"threeOfAKind",
	"straight",
	"flush",
	"fullHouse",
	"fourOfAKind",
	"straightFlush"
];

class Card {
	constructor(cardStr) {
		if (cardStr.length > 2) throw new Error(`Invalid Card ${cardStr}`);
		this.value = cardStr.substr(0, 1);
		this.suit = cardStr.substr(1, 1);
		this.score = VALUES.indexOf(this.value);
	}
	static sort(a, b) {
		if (a.score > b.score) {
			return -1;
		} else if (a.score < b.score) {
			return 1;
		} else {
			return 0;
		}
	}
}

export class PokerHand {
	constructor(handStr) {
		this.cards = handStr
			.split(" ")
			.map(c => new Card(c))
			.sort(Card.sort);
		if (this.cards.length !== 5) throw new Error("Invalid hand");
		this.score = this.cards[0].score; // Assuming the hands are compared just on highest card vvalue
		this.handValue = HAND_VALUES.indexOf(this.getHandValue());
	}
	/**
	 * Sequence of 5 cards in increasing value
	 * Ace can precede 2 and follow up King
	 */
	isStraight() {
		if (this.cards[0].score - 4 === this.cards[4].score) return true; // Ace follows king
		if (
			this.cards[0].score === 12 &&
			this.cards[4].score === 0 &&
			this.cards[1].score === 3
		)
			return true; // If Ace precedes a 2
		return false;
	}

	/**
	 * 5 cards of the same suit
	 */
	isFlush() {
		return this.cards.filter(c => c.suit === this.cards[0].suit).length === 5;
	}

	getHandValue() {
		const groups = this.cards.reduce((groups, x) => {
			(groups[x.score] = groups[x.score] || []).push(x);
			return groups;
		}, {});
		switch (Object.keys(groups).length) {
			case 2:
				if (Object.values(groups).filter(g => g.length === 4).length === 1) {
					return "fourOfAKind";
				} else {
					return "fullHouse";
				}
			case 3:
				if (Object.values(groups).filter(g => g.length === 3).length === 1) {
					return "threeOfAKind";
				} else {
					return "2pairs";
				}
			case 4:
				return "pair";
			default:
				const flush = this.isFlush();
				const straight = this.isStraight();
				if (straight && flush) {
					return "straightFlush";
				} else if (flush) {
					return "flush";
				} else if (straight) {
					return "straight";
				} else {
					return "highCard";
				}
		}
	}

	compareWith(otherHand) {
		if (this.handValue > otherHand.handValue) {
			return Result.WIN;
		} else if (this.handValue < otherHand.handValue) {
			return Result.LOSS;
		} else {
			// Compare Highest value card
			if (this.score > otherHand.score) {
				return Result.WIN;
			} else if (this.score < otherHand.score) {
				return Result.LOSS;
			} else {
				return Result.TIE;
			}
		}
	}
}

export const Result = {
	WIN: 1,
	LOSS: 2,
	TIE: 3
};

export default PokerHand;
